import 'package:flutter/material.dart'; // 导入了Material UI组件库

// https://book.flutterchina.club/chapter2/first_flutter_app.html

void main() => runApp(MyApp()); // 页面入口

// 在Flutter中，大多数东西都是widget，包括对齐(alignment)、填充(padding)和布局(layout)。
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TestDemo', // 应用名称
      theme: ThemeData(
        primarySwatch: Colors.blue, // 蓝色主题
      ),
      home: MyHomePage(title: 'Hello Flutter'), // 应用首页路由
    );
  }
}

// 测试页面
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// 构建UI界面
class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0; // 计数，默认是0
  // 计数方法，每次+1
  void _incrementCounter() {
    // 改变状态:通知Flutter框架刷新UI
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Scaffold 是Material库中提供的页面脚手架，它包含导航栏和Body以及FloatingActionButton
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '你点击+按钮的次数:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      // 悬浮按钮
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

// 当右下角的floatingActionButton按钮被点击之后，会调用_incrementCounter
// 在_incrementCounter中，首先会自增_counter计数器（状态）
// 然后setState会通知Flutter框架状态发生变化
// Flutter会调用build方法以新的状态重新构建UI，最终显示在设备屏幕上。
